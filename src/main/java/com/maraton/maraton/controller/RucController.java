package com.maraton.maraton.controller;

import com.maraton.maraton.models.RucModel;
import com.maraton.maraton.service.RucService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
public class RucController {
    @Autowired
    RucService service;

    @PostMapping("/ruc")
    public RucModel ruc(@RequestBody RucModel ruc){
        return service.ruc(ruc);
    }
}
