package com.maraton.maraton.models;

import lombok.Data;

@Data
public class RucModel {
    String ruc;
    String razon_social;
    String estado;
    String direccion;
    String ubigeo;
    String departamento;
    String provincia;
    String distrito;
}
