package com.maraton.maraton.repository;

import com.maraton.maraton.models.RucModel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface DBrepository {
    @Insert("INSERT INTO ruc(ruc,razon_social,estado,direccion,ubigeo,departamento,provincia,distrito)\n" +
    "VALUES\n" +
    "(#{ruc.ruc},#{ruc.razon_social},#{ruc.estado},#{ruc.direccion},#{ruc.ubigeo},#{ruc.departamento},#{ruc.provincia},#{ruc.distrito});")
    public int saveData(@Param("ruc") RucModel ruc);
}
