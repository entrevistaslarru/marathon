package com.maraton.maraton.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.maraton.maraton.models.RucModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class WSRepository {
    @Autowired
    RestTemplate rest;
    public RucModel getData(String ruc) {
        String urlAdapter="http://wsruc.com/Ruc2WS_JSON.php?tipo=2&ruc="+ruc+"&token=cXdlcnR5bGFtYXJja19zYUBob3RtYWlsLmNvbXF3ZXJ0eQ==";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        HttpEntity entity = new HttpEntity<>( headers);
        RucModel objectNodeResponse = rest.exchange(urlAdapter, HttpMethod.POST, entity, RucModel.class).getBody();
        return objectNodeResponse;
    }
}
