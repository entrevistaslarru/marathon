package com.maraton.maraton.service;

import com.maraton.maraton.models.RucModel;
import com.maraton.maraton.repository.DBrepository;
import com.maraton.maraton.repository.WSRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RucService {
    @Autowired
    WSRepository ws_repository;
    @Autowired
    DBrepository dBrepository;
    public RucModel ruc(RucModel ruc) {
        RucModel reponseWS=ws_repository.getData(ruc.getRuc().trim());
        if (reponseWS.getRuc()!=null && !reponseWS.getRuc().isEmpty()){
            try{
                dBrepository.saveData(reponseWS);
            }catch (Exception e){}
        }
        return reponseWS;
    }
}
